import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/auth.dart';
import 'package:shop_app/providers/cart.dart';
import 'package:shop_app/providers/product.dart';
import 'package:shop_app/screens/product_detail_screen.dart';
import 'package:shop_app/widgets/product_card.dart';

class ProductItem extends StatelessWidget {
  // final String id;
  // final String title;
  // final String imageUrl;
  //
  // ProductItem(this.id, this.title, this.imageUrl);

  @override
  Widget build(BuildContext context) {
    final product = Provider.of<Product>(context, listen: false);
    final cart = Provider.of<Cart>(context, listen: false);
    final authData = Provider.of<Auth>(context, listen: false);

    return GridTile(
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamed(
            ProductDetailScreen.routeName,
            arguments: product.id,
          );
        },
        child: Hero(
            tag: product.id,
            child: ProductCard(
              bgColor: const Color(0xFFEEEEED),
              imageUrl: product.imageUrl,
              price: product.price.toInt(),
              title: product.title,
            )),
      ),
      header: GridTileBar(
        backgroundColor: Colors.transparent,
        title: SizedBox(width: double.infinity),
        leading: Consumer<Product>(
          builder: (ctx, productIn, _) => IconButton(
            icon: Icon(
              productIn.isFavorite ? Icons.favorite : Icons.favorite_border,
            ),
            onPressed: () {
              productIn.toggleFavoriteStatus(authData.token!, authData.userId!);
            },
            color: Color.fromARGB(255, 255, 0, 0),
          ),
        ),
        trailing: IconButton(
          icon: Icon(Icons.shopping_cart),
          onPressed: () {
            cart.addItem(product.id, product.price, product.title);
            ScaffoldMessenger.of(context).removeCurrentSnackBar();
            // ScaffoldMessenger.of(context).hideCurrentSnackBar();
            //create connection to the nearest Scaffold widget (that control page we're seeing)
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(
                  "Added item to cart!",
                ),
                action: SnackBarAction(
                  label: "UNDO",
                  onPressed: () {
                    cart.removeSingleItem(product.id);
                  },
                ),
                duration: Duration(seconds: 2),
              ),
            );
          },
          color: Color.fromARGB(255, 255, 251, 0),
        ),
      ),
    );
  }
}
