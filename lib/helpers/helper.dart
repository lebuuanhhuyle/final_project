import 'package:intl/intl.dart';

class Helper {
  static final customCurrencyFormat = NumberFormat.currency(
    locale: "vi",
    customPattern: '#.###',
  );
}
